import math


class Drone:
    def __init__(self, idx, coord, max_weight):
        self.idx = idx
        self.coord = coord
        self.max_weight = max_weight
        self.weight = 0

    def add_product(self, prod_weight, requested_amount):
        feasable_amount = int(math.floor((self.max_weight - self.weight) / prod_weight))
        sent_amount = requested_amount if feasable_amount >= requested_amount else feasable_amount
        self.weight += sent_amount * prod_weight
        return sent_amount

    def __str__(self):
        return 'Drone(id: {}, loc: {}, max_weight: {})'.format(self.idx, self.coord, self.max_weight)
