class Product:
    def __init__(self, idx, weigth):
        self.idx = idx
        self.weight = weigth
      
    def __lt__(self, other):
        return self.weight < other.weight
        
    def __eq__(self, other):
        return self.weight == other.weight
        
    def __repr__(self):
        return "Product(idx = %d, weight = %d)" % (self.idx, self.weight)
