from collections import defaultdict


class Order:
    def __init__(self, idx, coord, prod_list):
        self.idx = idx
        self.coord = coord
        prod_id_to_amnt = defaultdict(lambda: 0)

        for prod in prod_list:
            prod_id_to_amnt[prod] += 1

        self.prod_id_to_amnt = prod_id_to_amnt
        self.prod_num = len(self.prod_id_to_amnt)

    def __str__(self):
        return 'Order(idx: {}, loc:{}, prod_list: {})'.format(self.idx, self.coord, self.prod_id_to_amnt)