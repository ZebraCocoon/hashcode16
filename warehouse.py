class Warehouse:
    def __init__(self, idx, coord, prod_list):
        self.idx = idx
        self.coord = coord
        self.prod_list = prod_list

    def __str__(self):
        return 'Warehouse(idx: {}, warehouse coord: {}, prod_list: {})'.format(self.idx, self.coord, self.prod_list)
