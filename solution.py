import os
import sys

import math
from collections import defaultdict

DEBUG = True

MAX_TURNS = 0
ALL_PRODUCTS = []
ALL_WAREHOUSES = []
ALL_ORDERS = []
ALL_DRONES = []

from drone import Drone
from order import Order
from product import Product
from warehouse import Warehouse


def debug(msg):
    global DEBUG
    if DEBUG:
        print(msg)


def parse_line(line):
    return [int(x) for x in line.split(" ")]


def parse_input(in_path):
    global MAX_TURNS

    with open(in_path, 'r') as input_file:
        row_count, col_count, drone_count, MAX_TURNS, max_payload = parse_line(input_file.readline())
        product_types_count = parse_line(input_file.readline())[0]
        product_weights = parse_line(input_file.readline())
        assert len(product_weights) == product_types_count

        for idx in range(product_types_count):
            product = Product(idx, product_weights[idx])
            ALL_PRODUCTS.append(product)
            debug(product)

        warehouse_count = parse_line(input_file.readline())[0]
        for idx in range(warehouse_count):
            warehouse_coord = parse_line(input_file.readline())
            warehouse_prod_list = parse_line(input_file.readline())
            warehouse = Warehouse(idx, warehouse_coord, warehouse_prod_list)
            ALL_WAREHOUSES.append(warehouse)
            debug(str(warehouse))

        for idx in range(drone_count):
            drone = Drone(idx, ALL_WAREHOUSES[0].coord, max_payload)
            ALL_DRONES.append(drone)
            debug(drone)

        order_count = parse_line(input_file.readline())[0]
        for idx in range(order_count):
            order_coord = parse_line(input_file.readline())
            order_item_count = parse_line(input_file.readline())[0]
            order_items = parse_line(input_file.readline())
            assert len(order_items) == order_item_count
            order = Order(idx, order_coord, order_items)
            ALL_ORDERS.append(order)
            debug(str(order))


def distance(p1, p2):
    return int(math.ceil(math.sqrt(((p1[0] - p2[0]) ** 2) + ((p1[1] - p2[1]) ** 2))))

#def get_closest_warehouse


def solve_naive():

    def cmp_orders(o1, o2):
        d1 = distance(o1.coord, warehouse.coord)
        d2 = distance(o2.coord, warehouse.coord)
        if d1 < d2:
            return -1
        elif d2 > d1:
            return 1
        return 0

    commands = []

    #round robin
    drones_to_orders = defaultdict(lambda: [])
    sorted_orders = sorted(ALL_ORDERS, cmp=cmp_orders)
    for i in range(len(sorted_orders)):
        drone_index = i % len(ALL_DRONES)
        drones_to_orders[drone_index].append(ALL_ORDERS[i])

    for warehouse in ALL_WAREHOUSES:
        for drone in ALL_DRONES:
            for order in drones_to_orders[drone.idx]:
                warehouse_drone_turns = distance(warehouse.coord, drone.coord)
                drone_customer_turns = distance(order.coord, drone.coord)
                order_handle_turns = (2 * drone_customer_turns) + 2
                drone_order_turns = 0

                has_prods = True
                while has_prods and MAX_TURNS - drone_order_turns >= order_handle_turns:
                    relevant_prods = [prod_id for prod_id, prod_amnt in order.prod_id_to_amnt.items() if prod_amnt]
                    relevant_prods = [prod_id for prod_id in relevant_prods if warehouse.prod_list[prod_id]]
                    has_prods = len(relevant_prods) > 0

                    for prod_id in relevant_prods:
                        prod_amnt = order.prod_id_to_amnt[prod_id]
                        prod_amnt = prod_amnt if warehouse.prod_list[prod_id] >= prod_amnt else warehouse.prod_list[prod_id]
                        added_amnt = drone.add_product(ALL_PRODUCTS[prod_id].weight, prod_amnt)
                        if added_amnt:
                            commands.append('{drone_id} {drone_command} {warehouse_id} {product_id} {product_count}'
                                            .format(drone_id=drone.idx,
                                                    drone_command='L',
                                                    warehouse_id=warehouse.idx,
                                                    product_id=prod_id,
                                                    product_count=added_amnt))
                            commands.append('{drone_id} {drone_command} {order_id} {product_id} {product_count}'
                                            .format(drone_id=drone.idx,
                                                    drone_command='D',
                                                    order_id=order.idx,
                                                    product_id=prod_id,
                                                    product_count=added_amnt))

                            warehouse.prod_list[prod_id] -= added_amnt
                            order.prod_id_to_amnt[prod_id] -= added_amnt

                    if drone.weight:
                        drone_order_turns += order_handle_turns
                        drone.weight = 0

    commands = ['{}'.format(len(commands))] + commands
    return commands


    
if __name__ == "__main__":
    if len(sys.argv) != 2:
        print('usage: {} [quick|full]'.format(__file__))
        exit(1)

    solution_type = sys.argv[1]
    problems = []
    if solution_type == "quick":
        problems = ['mother_of_all_warehouses']
    elif solution_type == "full":
        problems = ['busy_day', 'mother_of_all_warehouses', 'redundancy']
    else:
        print("Please specify solution type - quick/full are supported")
        exit()

    for problem in problems:
        MAX_TURNS = 0
        ALL_PRODUCTS = []
        ALL_WAREHOUSES = []
        ALL_ORDERS = []
        ALL_DRONES = []
        print("solving naively for " + problem + "...")
        parse_input(os.path.join(os.path.dirname(__file__), "inputs", '{}.in'.format(problem)))
        output_lines = solve_naive()
        with open("{}.out".format(problem), 'w') as out_file:
            out_file.write('\n'.join(output_lines))
        print("done")
